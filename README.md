Android app for weather forecast fetching.

Used libraries: RxJava2, Retrofit2, Dagger, ButterKnife, Timber, Dexter, Play Services Location, Fresco and FacebookSDK.