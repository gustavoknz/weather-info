package kieling.weatherinfo;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;

import com.facebook.soloader.SoLoader;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import kieling.weatherinfo.main.MainViewModel;
import kieling.weatherinfo.main.MainViewModelFactory;
import kieling.weatherinfo.model.Clouds;
import kieling.weatherinfo.model.Main;
import kieling.weatherinfo.model.Rain;
import kieling.weatherinfo.model.Sys;
import kieling.weatherinfo.model.Weather;
import kieling.weatherinfo.model.WeatherData;
import kieling.weatherinfo.model.Wind;
import kieling.weatherinfo.networking.INetworkService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Local unit test for MainView, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class MainViewUnitTest {
    private static final WeatherData WEATHER_DATA = new WeatherData();
    private MainViewModel mMainViewModel;
    @Mock
    private INetworkService mService;
    @Rule
    public final InstantTaskExecutorRule mTaskExecutorRule = new InstantTaskExecutorRule();
    @Rule
    public final MockitoRule mMockitoRule = MockitoJUnit.rule();
    @Rule
    public final RxSchedulersOverrideRule mOverrideSchedulersRule = new RxSchedulersOverrideRule();

    @BeforeClass
    public static void before() {
        SoLoader.setInTestMode();
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        WEATHER_DATA.setCity("Atlantis");
        WEATHER_DATA.setClouds(new Clouds(1));
        WEATHER_DATA.setDateTime("");
        WEATHER_DATA.setMain(new Main(20.0));
        WEATHER_DATA.setRain(new Rain(2.0));
        WEATHER_DATA.setSys(new Sys("AT"));
        List<Weather> weather = new ArrayList<>();
        weather.add(new Weather(9));
        WEATHER_DATA.setWeather(weather);
        WEATHER_DATA.setWind(new Wind(5.0));
        when(mService.getWeatherDataServer(0, 0)).thenReturn(Single.just(WEATHER_DATA));
        mMainViewModel = new MainViewModelFactory(mService).create(MainViewModel.class);
    }

    @Test
    public void testLiveDataHasTheExpectedValue() {
        mMainViewModel.getWeatherData(0, 0);
        LiveData<WeatherData> liveData = mMainViewModel.getWeatherLiveData();
        WeatherData wd = liveData.getValue();
        assertEquals(WEATHER_DATA, wd);
    }
}
