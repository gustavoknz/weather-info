package kieling.weatherinfo.deps;

import javax.inject.Singleton;

import dagger.Component;
import kieling.weatherinfo.forecast.ForecastActivity;
import kieling.weatherinfo.main.MainActivity;
import kieling.weatherinfo.networking.NetworkModule;

@Singleton
@Component(modules = {NetworkModule.class})
public interface Deps {
    void inject(MainActivity mainActivity);

    void inject(ForecastActivity forecastActivity);
}
