package kieling.weatherinfo;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

import kieling.weatherinfo.deps.DaggerDeps;
import kieling.weatherinfo.deps.Deps;
import kieling.weatherinfo.networking.NetworkModule;

public class BaseActivity extends AppCompatActivity {
    Deps mDependencies;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        mDependencies = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();
    }

    protected Deps getDependencies() {
        return mDependencies;
    }
}
