package kieling.weatherinfo.app;

import androidx.multidex.MultiDexApplication;

import com.facebook.drawee.backends.pipeline.Fresco;

import kieling.weatherinfo.BuildConfig;
import timber.log.Timber;

public class MyApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        //Crashlytics
        //Fabric.with(this, new Crashlytics());

        //Fresco
        Fresco.initialize(getApplicationContext());

        //Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashlyticsReportingTree());
        }
    }
}
