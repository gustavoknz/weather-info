package kieling.weatherinfo.forecast;

import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.graphics.BlendModeColorFilterCompat;
import androidx.core.graphics.BlendModeCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import kieling.weatherinfo.BaseActivity;
import kieling.weatherinfo.R;
import kieling.weatherinfo.helpers.LocationRequester;
import kieling.weatherinfo.helpers.Utils;
import kieling.weatherinfo.helpers.WeatherLocationManager;
import kieling.weatherinfo.model.ForecastInfo;
import kieling.weatherinfo.networking.INetworkService;
import timber.log.Timber;

public class ForecastActivity extends BaseActivity implements ForecastView, LocationRequester {
    @Inject
    INetworkService mNetworkService;
    @BindView(R.id.forecastProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.forecastErrorText)
    TextView mErrorText;
    @BindColor(R.color.colorAccent)
    int mColorAccent;
    @BindView(R.id.forecastList)
    RecyclerView mForecastList;
    private ForecastPresenter mForecastPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);
        ButterKnife.bind(this);
        Timber.tag("ForecastActivity");
        getDependencies().inject(this);
        Utils.setActionBarWithBack(R.string.forecast_title, this);

        mForecastPresenter = new ForecastPresenter(mNetworkService, this);
        new WeatherLocationManager(this, this).getLocation();

        mProgressBar.getIndeterminateDrawable().setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(mColorAccent, BlendModeCompat.SRC_ATOP));

        mForecastList.setHasFixedSize(true);
        mForecastList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    @Override
    protected void onStop() {
        mForecastPresenter.onStop();
        super.onStop();
    }

    @Override
    public void showForecastWait() {
        Timber.d("in showWeatherWait...");
        mForecastList.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeForecastWait() {
        Timber.d("in removeForecastWait...");
        mForecastList.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onForecastFailure(String appErrorMessage) {
        Timber.d("Error forecast: %s", appErrorMessage);
        mErrorText.setVisibility(View.VISIBLE);
    }

    @Override
    public void onForecastSuccess(List<ForecastInfo> forecastInfo) {
        Timber.d("success: forecastInfo=%s", forecastInfo);
        if (forecastInfo.size() > 0) {
            RecyclerView.Adapter<ForecastAdapter.ViewHolder> drawerAdapter = new ForecastAdapter(getApplicationContext(), forecastInfo);
            mForecastList.setAdapter(drawerAdapter);
        } else {
            mErrorText.setVisibility(View.VISIBLE);
        }
    }

    public void backClicked(View view) {
        finish();
    }

    @Override
    public void onLocationAcquired(Location location) {
        mForecastPresenter.getForecastData(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onPermissionDenied() {
        showLocationError(R.string.location_permission_denied);
    }

    @Override
    public void onLocationError() {
        showLocationError(R.string.location_error);
    }

    private void showLocationError(int msgId) {
        mErrorText.setText(msgId);
        mErrorText.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }
}
