package kieling.weatherinfo.forecast;

import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import kieling.weatherinfo.model.ForecastData;
import kieling.weatherinfo.model.ForecastInfo;
import kieling.weatherinfo.model.WeatherConditionTypes;
import kieling.weatherinfo.model.WeatherData;
import kieling.weatherinfo.networking.INetworkService;
import timber.log.Timber;

class ForecastPresenter {
    private final INetworkService mNetworkService;
    private final ForecastView mForecastView;
    private final CompositeDisposable mCompositeDisposable;
    private final DateFormat mDateFormatDay;
    private final DateFormat mDateFormatHour;
    private final DateFormat mDateFormatFromServer;

    ForecastPresenter(INetworkService networkService, ForecastView forecastView) {
        mNetworkService = networkService;
        mForecastView = forecastView;
        mCompositeDisposable = new CompositeDisposable();
        Timber.tag("MainPresenter");
        mDateFormatDay = new SimpleDateFormat("EEEE, MMMM d", Locale.getDefault());
        mDateFormatFromServer = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        mDateFormatHour = new SimpleDateFormat("HH:mm", Locale.getDefault());
    }

    void getForecastData(double lat, double lon) {
        mForecastView.showForecastWait();
        mCompositeDisposable.add(mNetworkService.getForecastData(lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn((Throwable t) -> {
                    Timber.e(t, "Error fetching forecast data");
                    mForecastView.onForecastFailure("Error fetching forecast data");
                    return new ForecastData();
                })
                .doOnError(throwable -> Timber.e(throwable, "Error fetching forecast data (doOnError)"))
                .map(forecastData -> {
                    Timber.d("Received forecastData=%s", forecastData);
                    List<ForecastInfo> forecastInfo = new ArrayList<>();
                    ForecastInfo fi;
                    Date lastDateDay = null;
                    Date dateServer;
                    for (WeatherData wd : forecastData.getList()) {
                        fi = new ForecastInfo();
                        dateServer = mDateFormatFromServer.parse(wd.getDateTime());
                        if (dateServer != null && (lastDateDay == null || (mDateFormatFromServer.parse(wd.getDateTime()) != null && !DateUtils.isSameDay(dateServer, lastDateDay)))) {
                            lastDateDay = mDateFormatFromServer.parse(wd.getDateTime());
                            fi.setDateDay(mDateFormatDay.format(dateServer));
                        }
                        if (dateServer != null) {
                            fi.setDateHour(mDateFormatHour.format(dateServer));
                        }
                        if (wd.getWeather() != null && wd.getWeather().size() > 0) {
                            fi.setImageId(WeatherConditionTypes.getDrawableId(wd.getWeather().get(0).getId()));
                        }
                        if (wd.getMain() != null) {
                            fi.setTemperature(wd.getMain().getTemp());
                        }
                        if (wd.getRain() != null) {
                            fi.setRain(wd.getRain().getThreeHours());
                        }
                        if (wd.getWind() != null) {
                            fi.setWind(wd.getWind().getSpeed());
                        }
                        Timber.d("Adding %s", fi);
                        forecastInfo.add(fi);
                    }
                    return forecastInfo;
                })
                .subscribe(forecastInfo -> {
                    mForecastView.removeForecastWait();
                    mForecastView.onForecastSuccess(forecastInfo);
                }));
    }

    void onStop() {
        mCompositeDisposable.clear();
    }
}
