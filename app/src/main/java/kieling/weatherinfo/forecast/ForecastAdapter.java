package kieling.weatherinfo.forecast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kieling.weatherinfo.R;
import kieling.weatherinfo.model.ForecastInfo;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {
    private final Context mContext;
    private final List<ForecastInfo> mForecastInfoList;

    ForecastAdapter(Context context, List<ForecastInfo> forecastInfo) {
        mContext = context;
        mForecastInfoList = forecastInfo;
    }

    @NonNull
    @Override
    public ForecastAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_forecast, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastAdapter.ViewHolder holder, int position) {
        if (mForecastInfoList.get(position).getDateDay() == null) {
            holder.dayView.setVisibility(View.GONE);
        } else {
            holder.dayView.setVisibility(View.VISIBLE);
            holder.dayView.setText(mForecastInfoList.get(position).getDateDay());
        }
        holder.hourView.setText(mForecastInfoList.get(position).getDateHour());
        holder.imageView.setBackgroundResource(mForecastInfoList.get(position).getImageId());
        holder.temperatureView.setText(mContext.getString(R.string.forecast_item_temperature_value, mForecastInfoList.get(position).getTemperature()));
        Double rain = mForecastInfoList.get(position).getRain();
        if (rain == null) {
            holder.rainView.setText(mContext.getString(R.string.forecast_item_rain_zero_value));
        } else {
            holder.rainView.setText(mContext.getString(R.string.forecast_item_rain_value, rain));
        }
        Double wind = mForecastInfoList.get(position).getWind();
        if (wind == null) {
            holder.windView.setText(mContext.getString(R.string.forecast_item_wind_zero_value));
        } else {
            holder.windView.setText(mContext.getString(R.string.forecast_item_wind_value, wind));
        }
    }

    @Override
    public int getItemCount() {
        return mForecastInfoList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.forecastItemDay)
        TextView dayView;
        @BindView(R.id.forecastItemHour)
        TextView hourView;
        @BindView(R.id.forecastItemImage)
        ImageView imageView;
        @BindView(R.id.forecastItemTemperature)
        TextView temperatureView;
        @BindView(R.id.forecastItemRain)
        TextView rainView;
        @BindView(R.id.forecastItemWind)
        TextView windView;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
