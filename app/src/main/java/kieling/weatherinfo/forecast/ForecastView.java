package kieling.weatherinfo.forecast;

import java.util.List;

import kieling.weatherinfo.model.ForecastInfo;

interface ForecastView {
    void showForecastWait();

    void removeForecastWait();

    void onForecastFailure(String appErrorMessage);

    void onForecastSuccess(List<ForecastInfo> forecastInfo);
}
