package kieling.weatherinfo.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import kieling.weatherinfo.model.WeatherData;
import kieling.weatherinfo.networking.INetworkService;
import timber.log.Timber;

public class MainViewModel extends ViewModel {
    private static final String TAG = Class.class.getName();
    private final MutableLiveData<WeatherData> mWeatherData;
    private final CompositeDisposable mCompositeDisposable;
    private final INetworkService mService;

    MainViewModel(INetworkService service) {
        Timber.tag(TAG);
        mCompositeDisposable = new CompositeDisposable();
        mWeatherData = new MutableLiveData<>();
        mService = service;
    }

    public LiveData<WeatherData> getWeatherLiveData() {
        return mWeatherData;
    }

    public void getWeatherData(double lat, double lon) {
        mCompositeDisposable.add(
                mService.getWeatherDataServer(lat, lon)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .onErrorReturn(throwable -> {
                            Timber.e(throwable, "Error fetching weather data");
                            mWeatherData.postValue(new WeatherData());
                            return new WeatherData();
                        })
                        .doOnError(throwable -> Timber.e(throwable, "Error fetching weather data on doOnError"))
                        .subscribe(mWeatherData::postValue));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mCompositeDisposable.clear();
    }
}
