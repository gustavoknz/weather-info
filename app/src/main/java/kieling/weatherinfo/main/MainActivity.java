package kieling.weatherinfo.main;

import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.graphics.BlendModeColorFilterCompat;
import androidx.core.graphics.BlendModeCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.Profile;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.UpdateAvailability;

import java.util.Collections;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kieling.weatherinfo.BaseActivity;
import kieling.weatherinfo.privacypolicy.PrivacyPolicyActivity;
import kieling.weatherinfo.R;
import kieling.weatherinfo.forecast.ForecastActivity;
import kieling.weatherinfo.helpers.LocationRequester;
import kieling.weatherinfo.helpers.Utils;
import kieling.weatherinfo.helpers.WeatherLocationManager;
import kieling.weatherinfo.model.WeatherConditionTypes;
import kieling.weatherinfo.model.WeatherData;
import kieling.weatherinfo.networking.INetworkService;
import timber.log.Timber;

import static com.google.android.play.core.install.model.AppUpdateType.FLEXIBLE;
import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;

public class MainActivity extends BaseActivity implements LocationRequester {
    private static final String TAG = "MainActivity";
    private static final int MY_APP_UPDATE_REQUEST_CODE = 100;
    @Inject
    INetworkService mService;
    @BindView(R.id.mainDrawerLayout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.mainWeatherImage)
    ImageView mWeatherImage;
    @BindView(R.id.mainLocationValue)
    TextView mLocationValue;
    @BindView(R.id.mainTemperatureValue)
    TextView mTemperatureValue;
    @BindView(R.id.mainWindValue)
    TextView mWindValue;
    @BindView(R.id.mainRainValue)
    TextView mRainValue;
    @BindView(R.id.mainCloudsValue)
    TextView mCloudsValue;
    @BindView(R.id.mainContent)
    RelativeLayout mMainContent;
    @BindView(R.id.mainProgressBar)
    ProgressBar mProgressBar;
    @BindColor(R.color.colorAccent)
    int mColorAccent;
    @BindView(R.id.mainErrorText)
    TextView mErrorText;
    @BindView(R.id.login_button)
    LoginButton mLoginButton;
    @BindView(R.id.mainDrawerProfileImage)
    SimpleDraweeView mDrawerProfileImage;
    @BindView(R.id.mainDrawerProfileName)
    TextView mDrawerProfileName;
    @BindView(R.id.mainDrawerLogoutLayout)
    LinearLayout mDrawerLogoutLayout;
    @BindView(R.id.mainDrawerPrivacyPolicy)
    TextView mDrawerPrivacyPolicy;
    @BindView(R.id.mainDrawerVersion)
    TextView mDrawerVersion;
    private AccessTokenTracker mAccessTokenTracker;
    private ActionBarDrawerToggle mDrawerToggle;
    private AppUpdateManager mAppUpdateManager;
    private MainViewModel mMainViewModel;
    private CallbackManager mCallbackManager;
    private final Observer<WeatherData> mWeatherDataObserver = weatherData -> {
        onWeatherSuccess(weatherData);
        removeWeatherWait();
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Timber.tag(TAG);
        getDependencies().inject(this);
        Utils.setActionBarWithHamburger(R.string.app_name, this);

        Timber.d("Init onCreate MainActivity");
        MainViewModelFactory mainViewModelFactory = new MainViewModelFactory(mService);
        mMainViewModel = mainViewModelFactory.create(MainViewModel.class);
        mMainViewModel.getWeatherLiveData().observe(this, mWeatherDataObserver);
        new WeatherLocationManager(this, this).getLocation();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.main_drawer_open, R.string.main_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Timber.d("onDrawerOpened");
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Timber.d("onDrawerClosed");
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mProgressBar.getIndeterminateDrawable().setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(mColorAccent, BlendModeCompat.SRC_ATOP));

        mCallbackManager = CallbackManager.Factory.create();
        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Timber.d("onLogout caught; currentAccessToken: %s", currentAccessToken);
                if (currentAccessToken == null) {
                    Timber.d("onLogout caught");
                    setProfileInfo();
                }
            }
        };

        // Callback registration
        mLoginButton.setPermissions(Collections.singletonList("public_profile"));
        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Timber.d("onSuccess: %s", loginResult.getAccessToken());
                GraphRequest gr = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {
                            Timber.d("Response: %s", response.toString());
                            setProfileInfo();
                        });
                gr.executeAsync();
            }

            @Override
            public void onCancel() {
                Timber.d("onCancel");
            }

            @Override
            public void onError(FacebookException fe) {
                Timber.d(fe, "onError");
            }
        });
        setProfileInfo();
        mDrawerPrivacyPolicy.setPaintFlags(mDrawerPrivacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mDrawerVersion.setText(getVersionName());
        mAppUpdateManager = AppUpdateManagerFactory.create(MainActivity.this);
    }

    private void setProfileInfo() {
        Profile profile = Profile.getCurrentProfile();
        if (Profile.getCurrentProfile() == null) {
            mDrawerProfileImage.setImageResource(R.mipmap.ic_launcher_round);
            mDrawerProfileName.setVisibility(View.GONE);
            mDrawerProfileName.setText("");
            mDrawerLogoutLayout.setVisibility(View.GONE);
        } else {
            Uri imageUrl = profile.getProfilePictureUri(100, 100);
            Timber.i("Login ProfilePic: %s", imageUrl);
            mDrawerProfileImage.setImageURI(imageUrl);
            mDrawerProfileName.setVisibility(View.VISIBLE);
            mDrawerProfileName.setText(getString(R.string.main_drawer_profile_full_name, profile.getFirstName(), profile.getLastName()));
            mDrawerLogoutLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_APP_UPDATE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Timber.d("Next activity... doNextActivity");
                Toast.makeText(MainActivity.this, "App updated successfully", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this, "Update failed! Result code: " + resultCode, Toast.LENGTH_LONG).show();
                checkAppVersion(mAppUpdateManager);
            }
        } else {
            Timber.d("onActivityResult. requestCode: %d; resultCode: %d; data: %s", requestCode, resultCode, data.getExtras());
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.drawerPane)
    public void drawerClicked() {
        Timber.d("drawerClicked");
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void hamburgerClicked(View view) {
        Timber.d("Drawer clicked");
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle your other action bar items...
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        mAccessTokenTracker.startTracking();
        super.onResume();
        checkAppVersion(mAppUpdateManager);
    }

    @Override
    protected void onStop() {
        Timber.d("In onStop MainActivity");
        mMainViewModel.onCleared();

        // Stop FacebookSDK callback
        mAccessTokenTracker.stopTracking();
        super.onStop();
    }

    public void removeWeatherWait() {
        Timber.d("in removeWeatherWait...");
        mProgressBar.setVisibility(View.GONE);
        mMainContent.setVisibility(View.VISIBLE);
    }

    public void onWeatherFailure(String appErrorMessage) {
        Timber.d("Error main: %s", appErrorMessage);
        mErrorText.setVisibility(View.VISIBLE);
    }

    public void onWeatherSuccess(@Nullable WeatherData weatherData) {
        Timber.d("in onWeatherSuccess: %s", weatherData);
        if (weatherData == null) {
            Timber.wtf("Could not retrieve data");
            onWeatherFailure("Could not retrieve data");
            return;
        }
        if (weatherData.getWeather() != null && weatherData.getWeather().size() > 0) {
            int weatherId = weatherData.getWeather().get(0).getId();
            int drawableId = WeatherConditionTypes.getDrawableId(weatherId);
            Timber.d("The image id will be %d (%s)", drawableId, getResources().getResourceEntryName(drawableId));
            mWeatherImage.setBackgroundResource(drawableId);
        } else {
            Timber.i("Could not find main weather info");
        }
        if (weatherData.getCity() == null || weatherData.getSys() == null || weatherData.getSys().getCountry() == null) {
            mLocationValue.setVisibility(View.GONE);
        } else {
            mLocationValue.setText(getString(R.string.main_location_value, weatherData.getCity(), weatherData.getSys().getCountry()));
        }
        if (weatherData.getMain() == null || weatherData.getMain().getTemp() == null) {
            mTemperatureValue.setVisibility(View.GONE);
        } else {
            mTemperatureValue.setText(getString(R.string.main_temperature_value, weatherData.getMain().getTemp()));
        }
        if (weatherData.getWind() == null) {
            mWindValue.setVisibility(View.GONE);
        } else {
            mWindValue.setText(getString(R.string.main_wind_value, weatherData.getWind().getSpeed()));
        }
        if (weatherData.getRain() == null) {
            mRainValue.setVisibility(View.GONE);
        } else {
            mRainValue.setText(getString(R.string.main_rain_value, weatherData.getRain().getThreeHours()));
        }
        if (weatherData.getClouds() == null) {
            mCloudsValue.setVisibility(View.GONE);
        } else {
            mCloudsValue.setText(getString(R.string.main_clouds_value, weatherData.getClouds().getAll()));
        }
        if (mLocationValue.getVisibility() == View.GONE && mTemperatureValue.getVisibility() == View.GONE
                && mWindValue.getVisibility() == View.GONE && mWindValue.getVisibility() == View.GONE
                && mCloudsValue.getVisibility() == View.GONE) {
            mErrorText.setVisibility(View.VISIBLE);
            Timber.d("Showing error message");
        }
    }

    @OnClick(R.id.mainDrawerForecastLayout)
    public void forecastClicked() {
        startActivity(new Intent(getApplicationContext(), ForecastActivity.class));
    }

    @OnClick(R.id.mainDrawerLogoutLayout)
    public void logoutClicked() {
        LoginManager.getInstance().logOut();
        setProfileInfo();
    }

    @Override
    public void onLocationAcquired(Location location) {
        mMainViewModel.getWeatherData(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onPermissionDenied() {
        showLocationError(R.string.location_permission_denied);
    }

    @Override
    public void onLocationError() {
        showLocationError(R.string.location_error);
    }

    private void showLocationError(int msgId) {
        mErrorText.setText(msgId);
        mErrorText.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    @OnClick(R.id.mainDrawerPrivacyPolicy)
    public void privacyPolicyClicked() {
        startActivity(new Intent(MainActivity.this, PrivacyPolicyActivity.class));
    }

    private String getVersionName() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e, "Error fetching version name");
        }
        return "version not verified";
    }

    public void checkAppVersion(final AppUpdateManager appUpdateManager) {
        Timber.d("Checking app version...");
        Toast.makeText(MainActivity.this ,"Checking app version...", Toast.LENGTH_SHORT);
        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(appUpdateInfo -> {
                    Timber.d("Got appUpdateInfo = %s", appUpdateInfo);
                    if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                        try {
                            startUpdate(appUpdateManager, appUpdateInfo);
                        } catch (IntentSender.SendIntentException e) {
                            Timber.e(e, "Error updating");
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "No update is available", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(e -> {
                    Timber.e(e, "Failed to update");
                    Toast.makeText(MainActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                })
                .addOnCompleteListener(task -> Timber.d("doNextActivity Completed! %s", task));
    }

    private void startUpdate(AppUpdateManager appUpdateManager, AppUpdateInfo appUpdateInfo) throws IntentSender.SendIntentException {
        appUpdateManager.startUpdateFlowForResult(appUpdateInfo,
                appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE) ? IMMEDIATE : FLEXIBLE,
                MainActivity.this,
                MY_APP_UPDATE_REQUEST_CODE);
    }
}
