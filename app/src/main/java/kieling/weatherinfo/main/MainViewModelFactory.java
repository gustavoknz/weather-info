package kieling.weatherinfo.main;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import kieling.weatherinfo.networking.INetworkService;

public class MainViewModelFactory implements ViewModelProvider.Factory {
    private final INetworkService mNetworkService;

    public MainViewModelFactory(INetworkService networkService) {
        mNetworkService = networkService;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == MainViewModel.class) {
            //noinspection unchecked
            return (T) new MainViewModel(mNetworkService);
        }
        throw new IllegalArgumentException("The class should extends MainViewModel");
    }
}
