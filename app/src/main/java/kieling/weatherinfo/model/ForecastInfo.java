package kieling.weatherinfo.model;

import androidx.annotation.NonNull;

public class ForecastInfo {
    private String dateDay;
    private String dateHour;
    private Integer imageId;
    private Double temperature;
    private Double rain;
    private Double wind;

    public ForecastInfo() {
    }

    public String getDateDay() {
        return dateDay;
    }

    public void setDateDay(String dateDay) {
        this.dateDay = dateDay;
    }

    public String getDateHour() {
        return dateHour;
    }

    public void setDateHour(String dateHour) {
        this.dateHour = dateHour;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getRain() {
        return rain;
    }

    public void setRain(Double rain) {
        this.rain = rain;
    }

    public Double getWind() {
        return wind;
    }

    public void setWind(Double wind) {
        this.wind = wind;
    }

    public ForecastInfo(String dateDay, String dateHour, Integer imageId, Double temperature, Double rain, Double wind) {
        this.dateDay = dateDay;
        this.dateHour = dateHour;
        this.imageId = imageId;
        this.temperature = temperature;
        this.rain = rain;
        this.wind = wind;
    }

    @NonNull
    @Override
    public String toString() {
        return "ForecastInfo{" +
                "dateDay='" + dateDay + '\'' +
                ", dateHour='" + dateHour + '\'' +
                ", imageId=" + imageId +
                ", temperature=" + temperature +
                ", rain=" + rain +
                ", wind=" + wind +
                '}';
    }
}
