package kieling.weatherinfo.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Rain {
    @SerializedName("3h")
    private Double threeHours;

    public Double getThreeHours() {
        return threeHours;
    }

    public void setThreeHours(Double threeHours) {
        this.threeHours = threeHours;
    }

    public Rain(Double threeHours) {
        this.threeHours = threeHours;
    }

    @NonNull
    @Override
    public String toString() {
        return "Rain{" +
                "threeHours=" + threeHours +
                '}';
    }
}
