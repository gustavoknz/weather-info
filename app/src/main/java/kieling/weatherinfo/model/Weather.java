package kieling.weatherinfo.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Weather {
    @SerializedName("id")
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Weather(Integer id) {
        this.id = id;
    }

    @NonNull
    @Override
    public String toString() {
        return "Weather{" +
                "id=" + id +
                '}';
    }
}
