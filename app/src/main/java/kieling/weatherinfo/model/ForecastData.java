package kieling.weatherinfo.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ForecastData {
    @SerializedName("list")
    private List<WeatherData> list;

    public ForecastData() {
        this.list = new ArrayList<>();
    }

    public List<WeatherData> getList() {
        return list;
    }

    public void setList(List<WeatherData> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public String toString() {
        return "ForecastData{" +
                "list=" + list +
                '}';
    }
}
