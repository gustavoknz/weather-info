package kieling.weatherinfo.model;

import android.util.SparseIntArray;

import kieling.weatherinfo.R;

public class WeatherConditionTypes {
    private static final SparseIntArray conditions = new SparseIntArray();

    static {
        conditions.put(200, R.drawable.ic_thunderstorm);
        conditions.put(201, R.drawable.ic_thunderstorm);
        conditions.put(202, R.drawable.ic_thunderstorm);
        conditions.put(210, R.drawable.ic_thunderstorm);
        conditions.put(211, R.drawable.ic_thunderstorm);
        conditions.put(212, R.drawable.ic_thunderstorm);
        conditions.put(221, R.drawable.ic_thunderstorm);
        conditions.put(230, R.drawable.ic_thunderstorm);
        conditions.put(231, R.drawable.ic_thunderstorm);
        conditions.put(232, R.drawable.ic_thunderstorm);

        conditions.put(300, R.drawable.ic_drizzle);
        conditions.put(301, R.drawable.ic_drizzle);
        conditions.put(302, R.drawable.ic_drizzle);
        conditions.put(310, R.drawable.ic_drizzle);
        conditions.put(311, R.drawable.ic_drizzle);
        conditions.put(312, R.drawable.ic_drizzle);
        conditions.put(313, R.drawable.ic_drizzle);
        conditions.put(314, R.drawable.ic_drizzle);
        conditions.put(321, R.drawable.ic_drizzle);

        conditions.put(500, R.drawable.ic_rainy);
        conditions.put(501, R.drawable.ic_rainy);
        conditions.put(502, R.drawable.ic_rainy);
        conditions.put(503, R.drawable.ic_rainy);
        conditions.put(504, R.drawable.ic_rainy);
        conditions.put(511, R.drawable.ic_heavy_rainy);
        conditions.put(520, R.drawable.ic_heavy_rainy);
        conditions.put(521, R.drawable.ic_heavy_rainy);
        conditions.put(522, R.drawable.ic_heavy_rainy);
        conditions.put(531, R.drawable.ic_heavy_rainy);

        conditions.put(600, R.drawable.ic_snowy);
        conditions.put(601, R.drawable.ic_snowy);
        conditions.put(602, R.drawable.ic_snowy);
        conditions.put(611, R.drawable.ic_snowy);
        conditions.put(612, R.drawable.ic_snowy);
        conditions.put(615, R.drawable.ic_snowy);
        conditions.put(616, R.drawable.ic_snowy);
        conditions.put(620, R.drawable.ic_snowy);
        conditions.put(621, R.drawable.ic_snowy);
        conditions.put(622, R.drawable.ic_snowy);

        conditions.put(701, R.drawable.ic_misty);
        conditions.put(711, R.drawable.ic_misty);
        conditions.put(721, R.drawable.ic_misty);
        conditions.put(731, R.drawable.ic_misty);
        conditions.put(741, R.drawable.ic_misty);
        conditions.put(751, R.drawable.ic_misty);
        conditions.put(761, R.drawable.ic_misty);
        conditions.put(762, R.drawable.ic_misty);
        conditions.put(771, R.drawable.ic_misty);
        conditions.put(781, R.drawable.ic_misty);

        conditions.put(800, R.drawable.ic_sunny);
        conditions.put(801, R.drawable.ic_cloudy);
        conditions.put(802, R.drawable.ic_cloudy);
        conditions.put(803, R.drawable.ic_clouds_few);
        conditions.put(804, R.drawable.ic_clouds_few);

        conditions.put(900, R.drawable.ic_heavy_rainy);
        conditions.put(901, R.drawable.ic_heavy_rainy);
        conditions.put(902, R.drawable.ic_heavy_rainy);
        conditions.put(903, R.drawable.ic_heavy_rainy);
        conditions.put(904, R.drawable.ic_heavy_rainy);
        conditions.put(905, R.drawable.ic_windy);
        conditions.put(951, R.drawable.ic_sunny);
        conditions.put(952, R.drawable.ic_windy);
        conditions.put(953, R.drawable.ic_windy);
        conditions.put(954, R.drawable.ic_windy);
        conditions.put(955, R.drawable.ic_windy);
        conditions.put(956, R.drawable.ic_windy);
        conditions.put(957, R.drawable.ic_windy);
        conditions.put(958, R.drawable.ic_windy);
        conditions.put(959, R.drawable.ic_windy);
        conditions.put(960, R.drawable.ic_heavy_rainy);
        conditions.put(961, R.drawable.ic_heavy_rainy);
        conditions.put(962, R.drawable.ic_heavy_rainy);
    }

    public static int getDrawableId(Integer key) {
        return conditions.get(key, -1);
    }
}
