package kieling.weatherinfo.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Main {
    @SerializedName("temp")
    private Double temp;

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Main(Double temp) {
        this.temp = temp;
    }

    @NonNull
    @Override
    public String toString() {
        return "Main{" +
                "temp=" + temp +
                '}';
    }
}
