package kieling.weatherinfo.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Clouds {
    @SerializedName("all")
    private Integer all;

    public Integer getAll() {
        return all;
    }

    public void setAll(Integer all) {
        this.all = all;
    }

    public Clouds(Integer all) {
        this.all = all;
    }

    @NonNull
    @Override
    public String toString() {
        return "Clouds{" +
                "all=" + all +
                '}';
    }
}
