package kieling.weatherinfo.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Wind {
    @SerializedName("speed")
    private Double speed;

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Wind(Double speed) {
        this.speed = speed;
    }

    @NonNull
    @Override
    public String toString() {
        return "Wind{" +
                "speed=" + speed +
                '}';
    }
}
