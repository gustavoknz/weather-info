package kieling.weatherinfo.networking;

import io.reactivex.Single;
import kieling.weatherinfo.BuildConfig;
import kieling.weatherinfo.model.ForecastData;
import kieling.weatherinfo.model.WeatherData;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface INetworkService {
    @GET("weather?units=metric&APPID=" + BuildConfig.API_KEY)
    Single<WeatherData> getWeatherDataServer(@Query("lat") double lat,
                                             @Query("lon") double lon);

    @GET("forecast?units=metric&APPID=" + BuildConfig.API_KEY)
    Single<ForecastData> getForecastData(@Query("lat") double lat,
                                         @Query("lon") double lon);
}
