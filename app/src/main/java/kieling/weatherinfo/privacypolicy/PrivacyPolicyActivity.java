package kieling.weatherinfo.privacypolicy;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kieling.weatherinfo.BaseActivity;
import kieling.weatherinfo.R;
import kieling.weatherinfo.helpers.Utils;
import timber.log.Timber;

public class PrivacyPolicyActivity extends BaseActivity {
    @BindView(R.id.privacyPolicyWebView)
    WebView mWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        ButterKnife.bind(this);
        Timber.tag("PrivacyPolicyActivity");
        Utils.setActionBarWithBack(R.string.app_name, this);

        Timber.d("Loading privacy policy from file...");
        mWebView.loadUrl("file:///android_asset/privacy_policy.html");
    }

    public void backClicked(View view) {
        finish();
    }
}
