package kieling.weatherinfo.helpers;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import kieling.weatherinfo.R;

public class Utils {
    public static void setActionBarWithBack(int titleId, AppCompatActivity activity) {
        setActionBar(true, titleId, activity);
    }

    public static void setActionBarWithHamburger(int titleId, AppCompatActivity activity) {
        setActionBar(false, titleId, activity);
    }

    private static void setActionBar(boolean backButton, int titleId, AppCompatActivity activity) {
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.toolbar_layout);
            actionBar.setDisplayShowTitleEnabled(false);

            View actionBarView = actionBar.getCustomView();
            TextView titleTextView = actionBarView.findViewById(R.id.toolbarTitleText);
            titleTextView.setText(titleId);

            ImageView hamburgerView = actionBarView.findViewById(R.id.toolbarHamburgerImage);
            ImageView backView = actionBarView.findViewById(R.id.toolbarBackImage);
            if (backButton) {
                hamburgerView.setVisibility(View.GONE);
                backView.setVisibility(View.VISIBLE);
            } else {
                hamburgerView.setVisibility(View.VISIBLE);
                backView.setVisibility(View.GONE);
            }
            actionBar.show();
        }
    }
}
