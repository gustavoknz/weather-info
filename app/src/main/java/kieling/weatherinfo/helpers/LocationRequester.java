package kieling.weatherinfo.helpers;

import android.location.Location;

public interface LocationRequester {
    void onLocationAcquired(Location location);

    void onPermissionDenied();

    void onLocationError();
}
