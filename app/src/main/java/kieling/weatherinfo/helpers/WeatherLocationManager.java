package kieling.weatherinfo.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import kieling.weatherinfo.R;
import timber.log.Timber;

public class WeatherLocationManager {
    private final Activity mActivity;
    private final LocationRequester mLocationRequester;
    private FusedLocationProviderClient mFusedLocationClient;

    public WeatherLocationManager(Activity activity, LocationRequester locationRequester) {
        mActivity = activity;
        mLocationRequester = locationRequester;
        Timber.tag("WeatherLocationManager");
    }

    public void getLocation() {
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Timber.d("onPermissionGranted");
                if (mFusedLocationClient == null) {
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mActivity);
                }
                if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(mActivity, location -> {
                            // Got last known location. In some rare situations this can be null.
                            if (location == null) {
                                Timber.d("OMG! Location is null /o\\");
                                mLocationRequester.onLocationError();
                            } else {
                                double latitude = location.getLatitude();
                                double longitude = location.getLongitude();
                                Timber.d("GOT LOCATION; Latitude: %.8f; Longitude: %.8f", latitude, longitude);
                                mLocationRequester.onLocationAcquired(location);
                            }
                        });
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Timber.d("onPermissionDenied");
                mLocationRequester.onPermissionDenied();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, final PermissionToken token) {
                Timber.d("onPermissionRationaleShouldBeShown");
                new AlertDialog.Builder(mActivity)
                        .setTitle(R.string.location_dialog_permission_title)
                        .setMessage(R.string.location_dialog_permission_message)
                        .setPositiveButton(R.string.location_dialog_permission_positive_button,
                                (dialogInterface, i) -> token.continuePermissionRequest())
                        .create()
                        .show();
            }
        };
        Dexter.withContext(mActivity)
                .withPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(permissionListener)
                .check();
    }
}
