package kieling.weatherinfo;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import kieling.weatherinfo.main.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = ApplicationProvider.getApplicationContext();
        assertEquals("kieling.weatherinfo", appContext.getPackageName());
    }

    @Test
    public void checkViewsDisplay() {
        onView(withId(R.id.mainProgressBar)).check(matches(isDisplayed()));
        onView(withId(R.id.login_button)).check(matches(isDisplayed()));
    }

    @Test
    public void checkNavigationDrawer() {
        //Drawer should be hidden
        onView(withId(R.id.mainDrawerForecastLayout)).check(matches(not(isDisplayed())));
        //Hamburger should be shown
        onView(withId(R.id.toolbarHamburgerImage)).check(matches(isDisplayed()));
        //Click on hamburger
        onView(withId(R.id.toolbarHamburgerImage)).perform(click());
        //Hamburger should still be shown
        onView(withId(R.id.toolbarHamburgerImage)).check(matches(isDisplayed()));
        //Drawer should now be shown
        onView(withId(R.id.mainDrawerForecastLayout)).check(matches(isDisplayed()));
        //Hamburger should still be shown
        onView(withId(R.id.toolbarHamburgerImage)).check(matches(isDisplayed()));
        //Click on hamburger
        onView(withId(R.id.toolbarHamburgerImage)).perform(click());
        //Drawer should be hidden again
        onView(withId(R.id.mainDrawerForecastLayout)).check(matches(not(isDisplayed())));
    }
}
